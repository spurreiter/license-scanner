import semver from 'semver'
import { logger } from './logger.js'

const log = logger('utils')

/**
 * @param {any} any
 * @returns {boolean}
 */
const isObject = (any) => typeof any === 'object' && any && !Array.isArray(any)

/**
 * @param {any} any
 * @returns {boolean}
 */
const isString = (any) => typeof any === 'string'

/**
 * check if `value` is an empty object or array
 * @param {any} value
 * @returns {boolean}
 */
export const isEmptyObj = (value) =>
  value === null ||
  value === undefined ||
  (typeof value === 'object' && !Object.keys(value).length)

/**
 * @param {any} any
 * @returns {boolean}
 */
const isValidUrl = (any) => {
  try {
    const url = new URL(any)
    return !!url
  } catch (e) {
    return false
  }
}

/**
 * @param {string|{type: string, url: string, [key: string]: any}} repository
 * @returns {string|undefined}
 */
export const repositoryUrl = (repository) => {
  if (
    typeof repository === 'object' && // ts could do smarter
    isObject(repository) &&
    repository.url
  ) {
    const url = repository.url
    return repositoryUrl(url)
  }
  if (!isString(repository)) {
    return
  }
  let url = '' + repository
  if (repository.startsWith('github:')) {
    const repo = repository.slice(7)
    url = `https://github.com/${repo}`
  } else if (repository.startsWith('gist:')) {
    const repo = repository.slice(5)
    url = `https://gist.github.com/${repo}`
  } else if (repository.startsWith('bitbucket:')) {
    const repo = repository.slice(10)
    url = `https://bitbucket.org/${repo}`
  } else if (repository.startsWith('gitlab:')) {
    const repo = repository.slice(7)
    url = `https://gitlab.com/${repo}`
  } else if (/^[^:/]+[/][^/]+$/.test(url)) {
    url = `https://github.com/${url}`
  }
  if (isValidUrl(url)) {
    return sanitizeRepoUrl(url)
  }
}

const supportedProtocols = {
  'git:': 'https:',
  'git+https:': 'https:',
  'http:': 'https:'
}

/**
 * @see https://docs.npmjs.com/cli/v10/configuring-npm/package-json#urls-as-dependencies
 * @param {string} repoUrl
 * @returns {string|undefined}
 */
export const sanitizeRepoUrl = (repoUrl) => {
  try {
    if (!repoUrl) return
    if (repoUrl.startsWith('git+ssh://')) {
      repoUrl = 'https://' + repoUrl.slice(10).replace(':', '/')
    }
    let url = new URL(repoUrl)
    url.username = ''
    url.password = ''
    url.search = ''
    url.hash = ''
    if (Object.keys(supportedProtocols).includes(url.protocol)) {
      url = new URL('https://' + repoUrl.slice(url.protocol.length + 2))
    }
    if (url.protocol !== 'https:') {
      log.error(`unknown protocol=${repoUrl}`)
      return
    }
    if (url.pathname.endsWith('.git')) {
      url.pathname = url.pathname.slice(0, -4)
    }
    return url.toString()
  } catch (e) {
    log.error(e)
  }
}

/**
 * @typedef {string|{name: string, [key: string]: any}} People
 */
/**
 * @param {object} param
 * @param {People} param.author
 * @param {People[]} param.maintainers
 * @param {People[]} param.contributors
 * @param {number} [maxNames=3]
 * @returns {string|undefined}
 */
export const authorNames = (param, maxNames = 3) => {
  const { author, maintainers, contributors } = param || {}
  const names = new Set()
  const peoples = []

  if (author) peoples.push(author)
  for (const arr of [maintainers, contributors]) {
    if (Array.isArray(arr)) {
      arr.forEach((item) => peoples.push(item))
    }
  }

  let last = ''
  for (const people of peoples) {
    if (names.size >= maxNames) {
      last = ' and others'
      break
    }
    const name = authorName(people)
    if (name) names.add(name)
  }
  return [...names].join(', ') + last || undefined
}

/**
 * @param {People} author
 * @returns {string|undefined}
 */
const authorName = (author) => {
  if (!author) return
  if (
    typeof author === 'object' && // ts could do smarter
    isObject(author) &&
    typeof author.name === 'string'
  ) {
    return author.name
  }
  if (typeof author !== 'string') {
    return
  }
  const parts = author.split(/\s+/)
  const arr = []
  for (const part of parts) {
    if (/[<()]/.test(part[0])) {
      break
    }
    arr.push(part)
  }
  const name = arr.join(' ')
  return name
}

/**
 * @param {any|any[]} license
 * @returns {string}
 */
const getSpdxId = (license) => {
  if (!license) {
    return 'Unknown'
  }
  if (typeof license === 'string') {
    return license
  }
  if (typeof license === 'object' && license.type) {
    return license.type
  }
  if (Array.isArray(license)) {
    return getSpdxId(license[0])
  }
  return 'Unknown'
}

/**
 * @typedef {import('./types').Usage} Usage
 * @typedef {import('./types').Dependencies} Dependencies
 * @typedef {import('./types').License} License
 * @typedef {import('./types').Component} Component
 */

export const buildComponent = (pkgJson, usage, licenses = []) => {
  // @see https://docs.npmjs.com/cli/v10/configuring-npm/package-json
  const {
    name,
    version,
    description,
    license,
    licence, // these typos...
    licenses: licensesDeprecated, // some npm packages have this field
    homepage,
    author,
    maintainers,
    contributors,
    repository,
    dependencies
  } = pkgJson

  licenses[0] = {
    spdxId: getSpdxId(license || licence || licensesDeprecated),
    ...licenses[0]
  }

  const component = {
    name,
    version,
    description,
    licenses,
    homepage,
    author: authorNames({ author, maintainers, contributors }),
    repositoryUrl: repositoryUrl(repository),
    usage,
    dependencies
  }

  return component
}

const RE_MATCH_SEMVER = /([~^<>\d][<>=\d.x]{0,30})$/

/**
 * @param {string} range
 * @returns {string}
 */
export const getVersionRange = (range) => {
  if (semver.validRange(range)) {
    return range
  }
  const m = RE_MATCH_SEMVER.exec(range)
  if (m?.[0] && semver.validRange(m[0])) {
    return m[0]
  }
  return '*'
}
