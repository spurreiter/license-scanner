import * as path from 'path'
import * as semver from 'semver'
import {
  findPackageJson,
  readJsonFile,
  PKG_JSON,
  getLicenseFiles
} from './files.js'
import { pLimit } from './pLimit.js'
import { buildComponent, isEmptyObj, getVersionRange } from './utils.js'
import { logger } from './logger.js'

const log = logger('scanner')

/**
 * @typedef {import('./types.js').Component} Component
 * @typedef {import('./types.js').Dependencies} Dependencies
 * @typedef {import('./types.js').Filter} Filter
 */
/**
 * @typedef {Map<string, Map<string, object>>} PackagesMap
 */
/**
 * @typedef {{
 *  prod: boolean
 *  dev: boolean
 *  pkgJson: object
 *  filter?: Filter
 * }} ScanOptions
 * - prod: only scan for dependencies
 * - dev: only scan for devDependencies
 * - pkgJson: package JSON object
 */
/**
 * @typedef {{
 *  name: string
 *  version: string
 *  components: Component[]
 * }} ScanResult
 */

/**
 * @param {string} cwd - current working directory
 * @param {ScanOptions} [options]
 * @returns {Promise<ScanResult>}
 */
export const scanner = async (cwd, options) => {
  const { dev: _dev, prod: _prod, pkgJson: _pkgJson, filter } = options || {}

  const dev = _dev ?? !_prod
  const prod = _prod ?? !_dev

  // TODO: find closest node_modules with a package.json

  const files = await findPackageJson(cwd)
  if (!files?.length) {
    throw new Error(`no ${PKG_JSON} files found`)
  }

  const pkgsMap = await readPckgJsonFiles(files)

  const file = path.resolve(cwd, PKG_JSON)
  const pkgJson = _pkgJson || (await readJsonFile(file))
  const { name, version, dependencies, devDependencies } = pkgJson

  const components = []
  const found = new Set()
  if (prod) {
    await resolvePkgsInfo({
      name,
      version,
      pkgsMap,
      dependencies: getMaxSatisfyingVersions(pkgsMap, dependencies),
      components,
      filter,
      found
    })
  }
  if (dev) {
    await resolvePkgsInfo({
      name,
      version,
      pkgsMap,
      dependencies: getMaxSatisfyingVersions(pkgsMap, devDependencies),
      components,
      found,
      filter,
      usage: 'dev'
    })
  }

  return { name, version, /* size: components.length, */ components }
}

/**
 * @param {string[]} files
 * @param {number} [limit=50]
 * @returns {Promise<PackagesMap>}
 */
const readPckgJsonFiles = async (files, limit = 50) => {
  const map = new Map()
  const load = (location) => async () => {
    const value = await readJsonFile(location).catch((err) =>
      log.error('readPckgJson failed err=%s file=%s', err, location)
    )
    if (value && typeof value === 'object') {
      value.__location = path.dirname(location)
      const { name, version } = value
      if (!name) return
      const versionMap = map.get(name) || new Map()
      versionMap.set(version, value)
      map.set(name, versionMap)
    }
  }
  const fns = files.map((location) => load(location))
  await pLimit(limit, fns)
  return map
}

/**
 * @param {string} name
 * @param {string} version
 * @returns {string}
 */
const nameVersionId = (name, version) => `${name}@${version}`

/**
 * @typedef {Set<string>} FoundComponents
 */

/**
 * @param {object} param
 * @param {string} param.name
 * @param {string} param.version
 * @param {Dependencies} [param.dependencies]
 * @param {PackagesMap} param.pkgsMap
 * @param {Component[]} param.components
 * @param {Filter} [param.filter]
 * @param {string} [param.usage]
 * @param {FoundComponents} [param.found]
 * @returns {Promise<>}
 */
export const resolvePkgsInfo = async (param) => {
  const {
    name: pckName,
    version: pckVersion,
    dependencies = {},
    pkgsMap,
    components,
    filter = {},
    usage,
    found = new Set()
  } = param

  const isFiltered = satisfies(pckVersion, filter?.[pckName]?.['*'])
  if (isFiltered) {
    log.debug('pkg filter=%s@%s', pckName, pckVersion, filter?.[pckName])
    return
  }
  if (!dependencies || typeof dependencies !== 'object') {
    return
  }

  for (const [name, version] of Object.entries(dependencies)) {
    const isFiltered =
      satisfies(version, filter?.[pckName]?.[name]) ||
      satisfies(version, filter?.['*']?.[name]) ||
      satisfies(version, filter?.[name]?.['*'])
    if (isFiltered) {
      log.debug('dep filter=%s@%s', name, version, pckName)
      continue
    }
    const versionsMap = pkgsMap.get(name)
    if (!versionsMap) {
      log.error(`No version found for ${nameVersionId(name, version)}`)
      continue
    }
    const id = nameVersionId(name, version)
    if (found.has(id)) {
      // prevent circularities
      continue
    }

    const pkgJson = versionsMap.get(version)
    if (!pkgJson || typeof pkgJson !== 'object') {
      log.error(name, version, versionsMap)
      continue
    }

    const licenses = await getLicenseFiles(pkgJson.__location)
    const component = buildComponent(pkgJson, usage, licenses)
    component.dependencies = getMaxSatisfyingVersions(
      pkgsMap,
      pkgJson.dependencies
    )
    components.push(component)
    found.add(id)

    if (!isEmptyObj(component.dependencies)) {
      // recurse into next dependencies
      await resolvePkgsInfo({
        name,
        version,
        dependencies: component.dependencies,
        pkgsMap,
        components,
        filter,
        usage,
        found
      })
    }
  }
}

/**
 * @param {PackagesMap} pkgsMap
 * @param {Dependencies} dependencies
 * @returns {Dependencies|undefined}
 */
const getMaxSatisfyingVersions = (pkgsMap, dependencies) => {
  if (!dependencies) return
  const deps = {}
  for (const [name, range] of Object.entries(dependencies)) {
    const versionsMap = pkgsMap.get(name)
    if (!versionsMap) {
      log.error(`No version found for ${nameVersionId(name, range)}`)
      continue
    }
    const versions = [...versionsMap.keys()]
    const _range = getVersionRange(range)

    const version = semver.maxSatisfying(versions, _range)
    deps[name] = version
  }
  return deps
}

/**
 * @param {string} version
 * @param {string|boolean} range
 * @returns {boolean}
 */
const satisfies = (version, range) => {
  if (!range) {
    return false
  }
  if (range === true) {
    return true
  }
  return semver.satisfies(version, range)
}
