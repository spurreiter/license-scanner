import * as fsp from 'fs/promises'
import * as path from 'path'
import { glob } from 'glob'
import { pLimit } from './pLimit.js'

const UTF_8 = 'utf-8'

/**
 * @param {string|URL} filename
 * @returns {Promise<Record<any, any>>}
 */
export const readJsonFile = async (filename) => {
  const content = await fsp.readFile(filename, UTF_8)
  return JSON.parse(content)
}

/**
 * @param {string|URL} filename
 * @param {any} obj
 * @returns {Promise<void>}
 */
export const writeJsonFile = async (filename, obj) => {
  await fsp.writeFile(filename, JSON.stringify(obj, null, 2), UTF_8)
}

export const PKG_JSON = 'package.json'

/**
 * @param {string} cwd
 * @returns {Promise<string[]|undefined>}
 */
export const findPackageJson = async (cwd) => {
  const found = await glob('**/' + PKG_JSON, { cwd, follow: true, dot: true })
  if (!found?.length) {
    return
  }
  return found.map((f) => path.join(cwd, f))
}

const pkgMgrs = {
  'package-lock.json': 'npm',
  'yarn.lock': 'yarn',
  'pnpm-lock.yaml': 'pnpm'
}
/**
 * find package manager from lock file
 * defaults to 'npm', if no lock file is found
 * @param {string} cwd
 * @returns {Promise<string>}
 */
export const getPackageMgr = async (cwd) => {
  const found = await glob(Object.keys(pkgMgrs), { cwd })
  return pkgMgrs[found[0]] || 'npm'
}

const licenseFiles = /license/i
/**
 * @typedef {{
 *  location: string
 *  text: string
 * }} LicenseFile
 * - location: relative location license info
 * - text: extracted text
 */
/**
 * @param {string} cwd
 * @param {import('glob').GlobOptionsWithFileTypesUnset} [opts]
 * @returns {Promise<LicenseFile[]>}
 */
export const getLicenseFiles = async (cwd, opts) => {
  const found = (await glob('*', {
    ...opts,
    cwd,
    ignore: ['node_modules/**']
  })).filter((f) => licenseFiles.test(f))

  const fns = found.map((location) => {
    const filename = path.resolve(cwd, location)
    return () => fsp.readFile(filename, UTF_8)
  })
  const results = await pLimit(50, fns)

  return found
    .map((location, i) => {
      const { status, value } = results[i]
      const text = status === 'fulfilled' ? value : undefined
      return {
        location,
        text
      }
    })
    .sort((a, b) => a.location.length - b.location.length)
}
