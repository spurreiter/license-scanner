import { Log } from 'debug-level'

export const logger = (namespace, opts) =>
  new Log(`license-scanner:${namespace}`, opts)
