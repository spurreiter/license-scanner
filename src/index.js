/**
 * @typedef {import('./types').Usage} Usage
 * @typedef {import('./types').License} License
 * @typedef {import('./types').Dependencies} Dependencies
 * @typedef {import('./types').Component} Component
 * @typedef {import('./scanner.js').ScanOptions} ScanOptions
 * @typedef {import('./scanner.js').ScanResult} ScanResult
 */
import { getVersionRange, repositoryUrl, sanitizeRepoUrl } from './utils.js'
import { sanitizeSpdxId } from './spdx.js'

export const utils = {
  getVersionRange,
  repositoryUrl,
  sanitizeRepoUrl,
  sanitizeSpdxId
}
export { scanner } from './scanner.js'
