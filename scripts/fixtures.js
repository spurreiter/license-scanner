#!/usr/bin/env node

/* eslint no-console: off */

import path from 'node:path'
import { fileURLToPath } from 'url'
import { spawn } from 'node:child_process'

export function exec (command, opts = {}) {
  const [cmd, ...args] = command.split(/\s+/)
  const sub = spawn(cmd, args, opts)
  let stdout = Buffer.from('')
  let stderr = Buffer.from('')
  sub.stdout.on('data', (data) => {
    process.stdout.write(data)
    stdout = Buffer.concat([stdout, data])
  })
  sub.stderr.on('data', (data) => {
    process.stderr.write(data)
    stderr = Buffer.concat([stderr, data])
  })
  return new Promise((resolve, reject) => {
    sub.on('error', reject)
    sub.on('close', () => resolve(stdout))
  })
}

const __dirname = path.dirname(fileURLToPath(import.meta.url))

const fixturesAll = [
  'npm',
  'npm-workspace',
  'pnpm',
  'pnpm-workspace',
  'yarn',
  'yarn-workspace'
].map((dir) => path.resolve(__dirname, '../test/fixtures', dir))

const install = async () => {
  const cmds = [
    'corepack enable',
    'corepack prepare pnpm@* --activate',
    'corepack prepare yarn@4 --activate'
  ]
  for (const cmd of cmds) {
    await exec(cmd)
  }
  console.log('pnpm --version == %s', await exec('pnpm --version'))
  console.log('yarn --version == %s', await exec('yarn --version'))
  for (const cwd of fixturesAll) {
    exec('npm run clean', { cwd })
  }
}

const test = async () => {
  for (const cwd of fixturesAll) {
    exec('npm t', { cwd })
  }
}

const argv = process.argv.slice(2)

if (argv.some(arg => /^-h|--help$/.test(arg))) {
  console.log(`
  Usage:
    fixtures.js [options]

  options:
    -h, --help    this help text
    install       install all fixtures
    test          test fixtures
  `)
} else if (argv.includes('install')) {
  await install()
} else if (argv.includes('test')) {
  await test()
}
