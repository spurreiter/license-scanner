/**
 * Builds the spdxIds.js file;
 * Downloads license Ids and exceptions from spdx.org
 */

import { writeFileSync } from 'fs'

const urlLicenses = 'https://spdx.org/licenses/licenses.json'
const urlExceptions = 'https://spdx.org/licenses/exceptions.json'

const template = (name, ids) =>
  `export const ${name} = ${JSON.stringify(ids, null, 2)}\n`

const proc = async (url) => {
  const data = await fetch(url).then((res) => {
    if (!res.ok) {
      return null
    }
    return res.json()
  })

  const { licenseListVersion, licenses = [], exceptions = [], releaseDate } = data
  const ids = []
  const deprecatedIds = []
  for (const { licenseId, isDeprecatedLicenseId } of licenses) {
    if (isDeprecatedLicenseId) {
      deprecatedIds.push(licenseId)
    } else {
      ids.push(licenseId)
    }
  }
  for (const { licenseExceptionId, isDeprecatedLicenseId } of exceptions) {
    if (isDeprecatedLicenseId) {
      deprecatedIds.push(licenseExceptionId)
    } else {
      ids.push(licenseExceptionId)
    }
  }

  return [ids.sort(), deprecatedIds.sort(), licenseListVersion, releaseDate]
}

const [ids, deprecatedIds, licenseListVersion, releaseDate] = await proc(
  urlLicenses
)
const [exceptions, deprecatedExceptions, exceptionsListVersion, exceptionsDate] = await proc(urlExceptions)

const content = [
  `// from ${urlLicenses}`,
  `// v${licenseListVersion} ${releaseDate}\n`,
  template('ids', ids),
  template('deprecatedIds', deprecatedIds),
  'export const allIds = [...ids, ...deprecatedIds]\n',
  `// from ${urlExceptions}`,
  `// v${exceptionsListVersion} ${exceptionsDate}\n`,
  template('exceptions', exceptions),
  template('deprecatedExceptions', deprecatedExceptions),
  'export const allExceptions = [...exceptions, ...deprecatedExceptions]\n'
].join('\n')

writeFileSync(new URL('../src/spdxIds.js', import.meta.url), content, 'utf-8')
