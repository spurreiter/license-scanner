import * as path from 'path'
import { scanner } from '../src/scanner.js'
import { equalFixture } from './equalFixture.js'
import { deepEqual } from 'assert/strict'

const toFixturePath = (filename) =>
  path.resolve(new URL(`./fixtures/${filename}`, import.meta.url).pathname)

const getComponentNames = ({ components }) => components.map((c) => c.name)

describe('scanner', function () {
  it('scan npm repo', async function () {
    const dir = toFixturePath('npm')
    const actual = await scanner(dir, { prod: true })
    await equalFixture(actual, toFixturePath('scan-npm.json'))
  })

  it('scan npm repo and filter lodash.pick', async function () {
    const dir = toFixturePath('npm')
    const actual = await scanner(dir, {
      prod: true,
      filter: { '*': { 'lodash.pick': true } }
    })
    deepEqual(getComponentNames(actual), ['debug', 'ms'])
  })

  it('scan npm repo and filter lodash.pick by range', async function () {
    const dir = toFixturePath('npm')
    const actual = await scanner(dir, {
      prod: true,
      filter: { '*': { 'lodash.pick': '^4' } }
    })
    deepEqual(getComponentNames(actual), ['debug', 'ms'])
  })

  it('scan npm repo and don`t filter lodash.pick by range', async function () {
    const dir = toFixturePath('npm')
    const actual = await scanner(dir, {
      prod: true,
      filter: { '*': { 'lodash.pick': '^3' } }
    })
    deepEqual(getComponentNames(actual), ['debug', 'ms', 'lodash.pick'])
  })

  it('scan npm repo and filter debug.ms', async function () {
    const dir = toFixturePath('npm')
    const actual = await scanner(dir, {
      prod: true,
      filter: { debug: { ms: true } }
    })
    deepEqual(getComponentNames(actual), ['debug', 'lodash.pick'])
  })

  it('scan npm repo and filter @test/npm.debug', async function () {
    const dir = toFixturePath('npm')
    const actual = await scanner(dir, {
      prod: true,
      filter: { '@test/npm': { debug: true } }
    })
    deepEqual(getComponentNames(actual), ['lodash.pick'])
  })
})
