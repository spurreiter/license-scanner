import { equal } from 'assert/strict'
import {
  repositoryUrl,
  authorNames,
  getVersionRange,
  sanitizeRepoUrl
} from '../src/utils.js'

describe('utils', function () {
  describe('repositoryUrl', function () {
    it('string', () => {
      const url = 'https://foo.bar/user/repo'
      equal(repositoryUrl(url), url)
    })
    it('object', () => {
      const url = { type: 'git', url: 'https://foo.bar/user/repo' }
      equal(repositoryUrl(url), url.url)
    })
    it('user/repo', () => {
      equal(repositoryUrl('user/repo'), 'https://github.com/user/repo')
    })
    it('apollographql/react-apollo in object', () => {
      equal(
        repositoryUrl({ type: 'git', url: 'apollographql/react-apollo' }),
        'https://github.com/apollographql/react-apollo'
      )
    })
    it('github:user/repo', () => {
      equal(repositoryUrl('github:user/repo'), 'https://github.com/user/repo')
    })
    it('gist:f03df587f2323b50beb4250520089a9e', () => {
      equal(
        repositoryUrl('gist:f03df587f2323b50beb4250520089a9e'),
        'https://gist.github.com/f03df587f2323b50beb4250520089a9e'
      )
    })
    it('bitbucket:user/repo', () => {
      equal(
        repositoryUrl('bitbucket:user/repo'),
        'https://bitbucket.org/user/repo'
      )
    })
    it('gitlab:user/repo', () => {
      equal(repositoryUrl('gitlab:user/repo'), 'https://gitlab.com/user/repo')
    })
    it('unknown:user/repo', () => {
      equal(repositoryUrl('unknown:user/repo'), undefined)
    })
    it('invalid-repo', () => {
      equal(repositoryUrl('invalid-repo'), undefined)
    })
  })

  describe('authorName', function () {
    it('undefined', () => {
      equal(authorNames({}), undefined)
    })

    it('object', () => {
      const author = {
        name: 'Barney Rubble',
        email: 'b@rubble.com',
        url: 'http://barnyrubble.tumblr.com/'
      }
      equal(authorNames({ author }), author.name)
    })

    it('only name', () => {
      const author =
        'Barney Rubble <b@rubble.com> (http://barnyrubble.tumblr.com/)'
      equal(authorNames({ author }), 'Barney Rubble')
    })

    it('with contributors', () => {
      const obj = {
        author: 'Josh Junon <josh.junon@protonmail.com>',
        contributors: [
          'TJ Holowaychuk <tj@vision-media.ca>',
          'Nathan Rajlich <nathan@tootallnate.net> (http://n8.io)',
          'Andrew Rhyne <rhyneandrew@gmail.com>'
        ]
      }
      equal(
        authorNames(obj),
        'Josh Junon, TJ Holowaychuk, Nathan Rajlich and others'
      )
    })

    it('only contributors', () => {
      const obj = {
        contributors: [
          'TJ Holowaychuk <tj@vision-media.ca>',
          'Nathan Rajlich <nathan@tootallnate.net> (http://n8.io)',
          'Andrew Rhyne <rhyneandrew@gmail.com>'
        ]
      }
      equal(authorNames(obj), 'TJ Holowaychuk, Nathan Rajlich, Andrew Rhyne')
    })
  })

  describe('getVersionRange', function () {
    it('valid semver range', () => {
      const range = '~1.2.0'
      equal(getVersionRange(range), '~1.2.0')
    })

    it('from git url', () => {
      const range = 'git+https://github.com/sindresorhus/sdbm'
      equal(getVersionRange(range), '*')
    })

    it('from git url with version tag', () => {
      const range = 'git+https://github.com/sindresorhus/sdbm#v1.2.0'
      equal(getVersionRange(range), '1.2.0')
    })

    it('from git url with semver', () => {
      const range = 'git+https://github.com/sindresorhus/sdbm#semver:~1.2.0'
      equal(getVersionRange(range), '~1.2.0')
    })
  })

  describe('sanitizeRepoUrl', function () {
    it('undefined', function () {
      equal(sanitizeRepoUrl(), undefined)
    })

    it('sanitize', function () {
      equal(
        sanitizeRepoUrl('git://github.com/isaacs/node-lru-cache'),
        'https://github.com/isaacs/node-lru-cache'
      )
    })

    it('sanitize proto and ext', function () {
      equal(
        sanitizeRepoUrl('git+https://github.com/isaacs/fs.realpath.git'),
        'https://github.com/isaacs/fs.realpath'
      )
    })

    it('sanitize search', function () {
      equal(
        sanitizeRepoUrl('https://github.com/isaacs/fs.realpath.git?test=1'),
        'https://github.com/isaacs/fs.realpath'
      )
    })

    it('sanitize unknown schema', function () {
      equal(repositoryUrl('unknown:user/repo'), undefined)
    })

    it('sanitize git+ssh', function () {
      equal(
        sanitizeRepoUrl('git+ssh://git@github.com:npm/cli.git#v1.0.27'),
        'https://github.com/npm/cli'
      )
    })
  })
})
