import { deepEqual } from 'assert/strict'
import { omit } from '../index.js'

describe('test/omit', function () {
  it('ok', function () {
    deepEqual(omit(), { b: '2' })
  })
  it('ok 2', function () {
    deepEqual(omit({ a: 'hi', b: 'hej', c: 'run', d: 'doe' }), {
      b: 'hej',
      d: 'doe'
    })
  })
})
