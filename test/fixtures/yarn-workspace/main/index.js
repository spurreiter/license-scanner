import { pick } from '@test/pick'
import { omit } from '@test/omit'
import sdbm from 'sdbm'

export const main = () => {
  const obj = { a: 'hi', b: 'hej', c: 'all', d: 'world' }
  const hash = sdbm(JSON.stringify(obj))
  const a = pick(obj)
  const b = omit(obj)
  return { ...a, ...b, hash }
}
