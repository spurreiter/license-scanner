import _pick from 'lodash.pick'
import debug from 'debug'

const log = debug('@test/npm')

export const pick = (obj) => {
  const _obj = obj || { a: 1, b: '2', c: 3 }
  const res = _pick(_obj, ['a', 'c'])
  log(res)
  return res
}
