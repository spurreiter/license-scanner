import { deepEqual } from 'assert/strict'
import { pick } from '../index.js'

describe('test/pick', function () {
  it('ok', function () {
    deepEqual(pick(), { a: 1, c: 3 })
  })
  it('ok 2', function () {
    deepEqual(pick({ a: 'hi', b: 'hej', c: 'run' }), { a: 'hi', c: 'run' })
  })
})
