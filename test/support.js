import * as path from 'path'

export const getDirname = (importMetaUrl) =>
  path.resolve(new URL(importMetaUrl).pathname).dirname
