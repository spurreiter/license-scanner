import { equal } from 'assert/strict'
import {

  sanitizeSpdxId
} from '../src/spdx.js'

describe('spdx', function () {
  describe('sanitizeSpdxId', function () {
    it('empty', function () {
      equal(sanitizeSpdxId(), 'Unknown')
    })

    it('MIT', function () {
      equal(sanitizeSpdxId('MIT'), 'MIT')
    })

    it('MIT-OR-CC (fails)', function () {
      equal(sanitizeSpdxId('MIT-OR-CC'), 'Unknown')
    })

    it('(MIT OR ISC)', function () {
      equal(sanitizeSpdxId('(MIT OR ISC)'), '(MIT OR ISC)')
    })

    it('CDDL-1.0+', function () {
      equal(sanitizeSpdxId('CDDL-1.0+'), 'CDDL-1.0+')
    })

    it('CDDM-1.1+ (fails)', function () {
      equal(sanitizeSpdxId('CDDM-1.1+'), 'Unknown')
    })

    it('Foobar OR MIT (fails)', function () {
      equal(sanitizeSpdxId('Foobar OR MIT'), 'Unknown')
    })

    it('LGPL-2.1-only OR MIT', function () {
      equal(sanitizeSpdxId('LGPL-2.1-only OR MIT'), 'LGPL-2.1-only OR MIT')
    })

    it('LGPL-2.1-only OR MIT OR BSD-3-Clause', function () {
      equal(
        sanitizeSpdxId('LGPL-2.1-only  OR    MIT OR BSD-3-Clause'),
        'LGPL-2.1-only OR MIT OR BSD-3-Clause'
      )
    })

    it('LGPL-2.1-only AND MIT', function () {
      equal(
        sanitizeSpdxId('(LGPL-2.1-only AND MIT)'),
        '(LGPL-2.1-only AND MIT)'
      )
    })

    it('GPL-2.0-or-later WITH Bison-exception-2.2', function () {
      equal(
        sanitizeSpdxId('GPL-2.0-or-later WITH Bison-exception-2.2'),
        'GPL-2.0-or-later WITH Bison-exception-2.2'
      )
    })

    it('GPL-2.0-or-later WITH Bison-exception (fails)', function () {
      equal(
        sanitizeSpdxId('GPL-2.0-or-later WITH Bison-exception'),
        'Unknown'
      )
    })
  })
})
