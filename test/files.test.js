import { ok, equal } from 'assert/strict'
import {
  findPackageJson,
  getPackageMgr,
  getLicenseFiles
} from '../src/files.js'
import { deepEqual } from 'assert'

describe('files', function () {
  describe('findPackageJson', function () {
    ;[
      { type: 'npm', count: 121 },
      { type: 'pnpm', count: 360 },
      { type: 'yarn', count: 184 }
    ].forEach(({ type, count }) => {
      it(`${type}: shall find all files`, async function () {
        const npmFixturesDir = new URL(`./fixtures/${type}`, import.meta.url)
          .pathname
        const files = await findPackageJson(npmFixturesDir)
        equal(files.length, count)
        ok(files[0].startsWith(npmFixturesDir))
      })
    })
    ;[
      { type: 'npm', count: 131 },
      { type: 'pnpm', count: 380 },
      { type: 'yarn', count: 194 }
    ].forEach(({ type, count }) => {
      it(`${type}: shall find all files in workspaces`, async function () {
        const npmFixturesDir = new URL(
          `./fixtures/${type}-workspace`,
          import.meta.url
        ).pathname
        const files = await findPackageJson(npmFixturesDir)
        equal(files.length, count)
        ok(files[0].startsWith(npmFixturesDir))
      })
    })
  })

  describe('getPackageMgr', function () {
    ;[{ type: 'npm' }, { type: 'pnpm' }, { type: 'yarn' }].forEach(
      ({ type }) => {
        it(`${type}: shall find package manager`, async function () {
          const npmFixturesDir = new URL(
            `./fixtures/${type}-workspace`,
            import.meta.url
          ).pathname
          const manager = await getPackageMgr(npmFixturesDir)
          equal(manager, type)
        })
      }
    )
  })

  describe('getLicenseFiles', function () {
    it('shall find LICENSE-MIT.txt', async function () {
      const cwd = new URL('./fixtures/licenses/txt', import.meta.url).pathname
      const result = await getLicenseFiles(cwd)
      deepEqual(result, [
        { location: 'LICENSE-MIT.txt', text: 'LICENSE-MIT.txt\n' }
      ])
    })

    it('shall find license.md', async function () {
      const cwd = new URL('./fixtures/licenses/md', import.meta.url).pathname
      const result = await getLicenseFiles(cwd)
      deepEqual(result, [
        {
          location: 'license.md',
          text: 'md/license.md\n'
        }
      ])
    })

    it('shall find various and sort by length', async function () {
      const cwd = new URL('./fixtures/licenses', import.meta.url).pathname
      const result = await getLicenseFiles(cwd)
      deepEqual(result, [
        {
          location: 'LICENSE',
          text: 'LICENSE\n'
        },
        {
          location: 'license.md',
          text: 'license.md\n'
        },
        {
          location: 'license.test',
          text: 'license.test\n'
        }
      ])
    })
  })
})
