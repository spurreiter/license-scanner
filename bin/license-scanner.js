#!/usr/bin/env node

/* eslint no-console: off */

import path from 'path'
import fs from 'fs'
import { scanner } from '../src/index.js'

const helpText = `
Usage:
  license-scanner [options] [dirname with package.json]

options:
  -h, --help          this help
      --version       display version information
      --dev           include dev dependencies
      --prod          include prod dependencies
  -o, --output csv    display output as csv
`

/**
 * arguments parser
 * @param {string[]} args
 * @returns {Record<string, any>}
 */
export function argv (args) {
  const argv = expand(args || process.argv.slice(2))
  const cmd = { path: process.cwd() }

  while (argv.length) {
    const arg = argv.shift()

    switch (arg) {
      case '-h':
      case '--help':
        cmd.help = true
        break
      case '--version':
        cmd.version = true
        break
      case '--dev':
        cmd.dev = true
        break
      case '--prod':
        cmd.prod = true
        break
      case '-o':
      case '--output':
        cmd.output = nextArg(argv, true)
        break
      default:
        cmd.path = path.resolve(process.cwd(), arg)
    }
  }
  // set defaults
  if (!cmd.dev && !cmd.prod) {
    cmd.prod = cmd.dev = true
  }

  return cmd
}

/**
 * @param {string[]} argv
 * @returns {string[]}
 */
function expand (argv) {
  const nArgv = []
  for (const arg of argv) {
    if (/^-[a-z]+$/.test(arg)) {
      const shortArgs = arg.slice(1).split('')
      for (const short of shortArgs) {
        nArgv.push(`-${short}`)
      }
    } else {
      nArgv.push(arg)
    }
  }
  return nArgv
}

/**
 * @param {string[]} argv
 * @param {boolean} required
 * @returns {string|boolean|undefined}
 */
function nextArg (argv, required = false) {
  const next = argv[0]
  if (typeof next !== 'string' || next.indexOf('-') === 0) {
    return required ? undefined : true
  }
  return argv.shift()
}

/**
 * @returns {string}
 */
function version () {
  const { version } = JSON.parse(
    fs.readFileSync(new URL('../package.json', import.meta.url), 'utf-8')
  )
  return version
}

/**
 * @param {import('../src/index.js').ScanResult} scanResult
 * @returns {string} csv fomatted string
 */
function toCsv (scanResult) {
  const quote = (val) => {
    if (val.includes(',') || val.includes('"')) {
      return `"${val.replace(/"/g, '""')}"`
    }
    return val
  }

  const output = ['name,version,license,repository,homepage,author,description']

  for (const component of scanResult.components) {
    const {
      name,
      version = '',
      description = '',
      licenses = [],
      homepage = '',
      author = '',
      repositoryUrl = ''
    } = component

    for (const license of licenses) {
      const { spdxId = 'Unknown' } = license || {}
      output.push(
        [name, version, spdxId, repositoryUrl, homepage, author, description]
          .map((val) => quote('' + val))
          .join(',')
      )
    }
  }

  return output.join('\n')
}

async function main () {
  const cmd = argv()

  if (cmd.help) {
    console.log(helpText)
    return
  }
  if (cmd.version) {
    console.log(version())
    return
  }

  const { prod, dev, path } = cmd

  const scanResult = await scanner(path, { prod, dev })

  let content
  if (cmd.output === 'csv') {
    content = toCsv(scanResult)
  } else {
    content = JSON.stringify(scanResult, null, 2)
  }
  process.stdout.write(content)
}

main().catch((err) => console.error(err.message))
