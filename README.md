# license-scanner

A license scanner for npm projects 

> **NOTE**:  
> Projects using yarn@>=2 as package-manager only work with the `.yarnrc.yml`
> setting `nodeLinker: node-modules`  
> 
> Monorepos are not yet supported.

# Install

```sh
npm install gitlab:spurreiter/license-scanner#semver:^0.2.3
```

# Usage 

```js
import { scanner } from 'license-scanner'

const scanResult = await scanner('/path/to/npm/project', { prod: true, dev: true })
```
See [scan-npm.json](test/fixtures/scan-npm.json) for a scanResult.

# CLI

To use in terminal via CLI install globally with:

```sh
npm install --global gitlab:spurreiter/license-scanner#semver:^0.2.3
```

```
Usage:
  license-scanner [options] [dirname with package.json]

options:
  -h, --help          this help
      --version       display version information
      --dev           include dev dependencies
      --prod          include prod dependencies
  -o, --output csv    display output as csv
```

# Development

```sh 
pnpm i
# install fixtures
npm run fixtures
# run some tests on the fresh installed fixtures
npm run fixtures:test
# run the scanner tests
npm t
```

# License 

[MIT](./LICENSE) licensed


[LICENSE]: ./LICENSE
[npm-badge]: https://badgen.net/npm/v/@spurreiter/license-scanner
[npm]: https://www.npmjs.com/package/@spurreiter/license-scanner
[types-badge]: https://badgen.net/npm/types/@spurreiter/license-scanner
