export const ids: string[];
export const deprecatedIds: string[];
export const allIds: string[];
export const exceptions: string[];
export const deprecatedExceptions: string[];
export const allExceptions: string[];
