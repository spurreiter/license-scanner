export namespace utils {
    export { getVersionRange };
    export { repositoryUrl };
    export { sanitizeRepoUrl };
    export { sanitizeSpdxId };
}
export { scanner } from "./scanner.js";
export type Usage = import('./types').Usage;
export type License = import('./types').License;
export type Dependencies = import('./types').Dependencies;
export type Component = import('./types').Component;
export type ScanOptions = import('./scanner.js').ScanOptions;
export type ScanResult = import('./scanner.js').ScanResult;
import { getVersionRange } from './utils.js';
import { repositoryUrl } from './utils.js';
import { sanitizeRepoUrl } from './utils.js';
import { sanitizeSpdxId } from './spdx.js';
