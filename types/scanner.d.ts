export function scanner(cwd: string, options?: ScanOptions | undefined): Promise<ScanResult>;
export function resolvePkgsInfo(param: {
    name: string;
    version: string;
    dependencies?: import("./types.js").Dependencies | undefined;
    pkgsMap: PackagesMap;
    components: Component[];
    filter?: import("./types.js").Filter | undefined;
    usage?: string | undefined;
    found?: FoundComponents | undefined;
}): Promise<any>;
export type Component = import('./types.js').Component;
export type Dependencies = import('./types.js').Dependencies;
export type Filter = import('./types.js').Filter;
export type PackagesMap = Map<string, Map<string, object>>;
/**
 * - prod: only scan for dependencies
 * - dev: only scan for devDependencies
 * - pkgJson: package JSON object
 */
export type ScanOptions = {
    prod: boolean;
    dev: boolean;
    pkgJson: object;
    filter?: Filter;
};
export type ScanResult = {
    name: string;
    version: string;
    components: Component[];
};
export type FoundComponents = Set<string>;
