export function readJsonFile(filename: string | URL): Promise<Record<any, any>>;
export function writeJsonFile(filename: string | URL, obj: any): Promise<void>;
export const PKG_JSON: "package.json";
export function findPackageJson(cwd: string): Promise<string[] | undefined>;
export function getPackageMgr(cwd: string): Promise<string>;
export function getLicenseFiles(cwd: string, opts?: import("glob").GlobOptionsWithFileTypesUnset | undefined): Promise<LicenseFile[]>;
/**
 * - location: relative location license info
 * - text: extracted text
 */
export type LicenseFile = {
    location: string;
    text: string;
};
