export function isEmptyObj(value: any): boolean;
export function repositoryUrl(repository: string | {
    [key: string]: any;
    type: string;
    url: string;
}): string | undefined;
export function sanitizeRepoUrl(repoUrl: string): string | undefined;
export function authorNames(param: {
    author: People;
    maintainers: People[];
    contributors: People[];
}, maxNames?: number | undefined): string | undefined;
export function buildComponent(pkgJson: any, usage: any, licenses?: any[]): {
    name: any;
    version: any;
    description: any;
    licenses: any[];
    homepage: any;
    author: string | undefined;
    repositoryUrl: string | undefined;
    usage: any;
    dependencies: any;
};
export function getVersionRange(range: string): string;
export type People = string | {
    [key: string]: any;
    name: string;
};
export type Usage = import('./types').Usage;
export type Dependencies = import('./types').Dependencies;
export type License = import('./types').License;
export type Component = import('./types').Component;
