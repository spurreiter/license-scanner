export type Usage = 'dev' | 'ui'

export type Dependencies = Record<string, string> | {}

/**
 * Filter package dependencies from being excluded from the scan
 * @example filter by all `ms` dependencies in all packages
 * {'*':{ms:'*'}}
 * @example filter by all `ms` dependencies in all packages with version less than `<2`
 * {'*':{ms:'<2'}}
 * @example filter by all `ms` packages being a dependency of `debug` with version less than `<2`
 * {'debug':{ms:'<2'}}
 */
export type Filter = Record<string, Record<string, boolean>>

export interface License {
  /**
   * SPDX License Identifier
   * @see https://spdx.org/licenses or `Unknown`
   */
  spdxId: string
  /**
   * extracted license text from `location`
   */
  text?: string
  /**
   * license file location relative to package.json
   */
  location?: string
}

export interface Component {
  /**
   * component name
   */
  name: string
  /**
   * component version
   */
  version: string
  /**
   * component description
   */
  description?: string
  /**
   * list of license information
   */
  licenses: License[]
  /**
   * authors, maintainers and contributors
   * limited to 3 names
   */
  authors?: string
  /**
   * package homepage
   */
  homepage?: string
  /**
   * repository url
   */
  repositoryUrl?: string
  /**
   * usage
   */
  usage?: Usage
  /**
   * Record of dependencies by name, version
   */
  dependencies?: Dependencies
}
